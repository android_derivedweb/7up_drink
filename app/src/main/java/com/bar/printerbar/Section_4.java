package com.bar.printerbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;


public class Section_4 extends AppCompatActivity {

    TextView title1,title2,title3,title4,title5,title6,title7;
    TextView dec1,dec2,dec3,dec4,dec5,dec6,dec7;

    LinearLayout mLayout1,mLayout2,mLayout3,mLayout4,mLayout5,mLayout6,mLayout7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sec_4);

        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finaly change the color
        window.setStatusBarColor(ContextCompat.getColor(Section_4.this, R.color.teal_200));

       // AndroidNetworking.initialize(getApplicationContext());

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);
        title4 = findViewById(R.id.title4);
        title5 = findViewById(R.id.title5);
        title6 = findViewById(R.id.title6);
        title7 = findViewById(R.id.title7);

        dec1 = findViewById(R.id.dec1);
        dec2 = findViewById(R.id.dec2);
        dec3 = findViewById(R.id.dec3);
        dec4 = findViewById(R.id.dec4);
        dec5 = findViewById(R.id.dec5);
        dec6 = findViewById(R.id.dec6);
        dec7 = findViewById(R.id.dec7);

        mLayout1 = findViewById(R.id.mLayout1);
        mLayout2 = findViewById(R.id.mLayout2);
        mLayout3 = findViewById(R.id.mLayout3);
        mLayout4 = findViewById(R.id.mLayout4);
        mLayout5 = findViewById(R.id.mLayout5);
        mLayout6 = findViewById(R.id.mLayout6);
        mLayout7 = findViewById(R.id.mLayout7);


        Bundle extras = getIntent().getExtras();
        String mName = extras.getString("mName");
        String mMobile = extras.getString("mMobile");
        String mEmail = extras.getString("mEmail");
//        String mName = "name test";
//        String mMobile = "1010101010";
//        String mEmail = "john@gmail.com";

        mLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Section_4.this,Section_6.class)
                        .putExtra("mName",mName)
                        .putExtra("mMobile",mMobile)
                        .putExtra("mEmail",mEmail)
                        .putExtra("mDrink",title1.getText().toString())
                );
                finish();
            }
        });
        mLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Section_4.this,Section_6.class)
                        .putExtra("mName",mName)
                        .putExtra("mMobile",mMobile)
                        .putExtra("mEmail",mEmail)
                        .putExtra("mDrink",title2.getText().toString())
                );
                finish();
            }
        });
        mLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Section_4.this,Section_6.class)
                        .putExtra("mName",mName)
                        .putExtra("mMobile",mMobile)
                        .putExtra("mEmail",mEmail)
                        .putExtra("mDrink",title3.getText().toString())
                );
                finish();
            }
        });
        mLayout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Section_4.this,Section_6.class)
                        .putExtra("mName",mName)
                        .putExtra("mMobile",mMobile)
                        .putExtra("mEmail",mEmail)
                        .putExtra("mDrink",title4.getText().toString())
                );
                finish();
            }
        });
        mLayout5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Section_4.this,Section_6.class)
                        .putExtra("mName",mName)
                        .putExtra("mMobile",mMobile)
                        .putExtra("mEmail",mEmail)
                        .putExtra("mDrink",title5.getText().toString())
                );
                finish();
            }
        });
        mLayout6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Section_4.this,Section_6.class)
                        .putExtra("mName",mName)
                        .putExtra("mMobile",mMobile)
                        .putExtra("mEmail",mEmail)
                        .putExtra("mDrink",title6.getText().toString())
                );
                finish();
            }
        });

        mLayout7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Section_4.this,Section_6.class)
                        .putExtra("mName",mName)
                        .putExtra("mMobile",mMobile)
                        .putExtra("mEmail",mEmail)
                        .putExtra("mDrink",title7.getText().toString())
                );
                finish();
            }
        });

    }

}