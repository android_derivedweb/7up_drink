
package com.bar.printerbar.db;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bar.printerbar.R;


import java.util.ArrayList;
import java.util.Locale;

public class AdapterRecordList extends RecyclerView.Adapter<AdapterRecordList.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<SaveOrder> timeSlotModelArrayList;

    public AdapterRecordList(Context mContext, ArrayList<SaveOrder> notificatioModelArrayList, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.timeSlotModelArrayList = notificatioModelArrayList;


    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_details, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });
        holder.mName.setText(timeSlotModelArrayList.get(position).getName());
        holder.mDrink.setText(timeSlotModelArrayList.get(position).getDrink());
//        holder.mGarnish1.setText(timeSlotModelArrayList.get(position).getGarnish_One());
//        holder.mGarnish2.setText(timeSlotModelArrayList.get(position).getGarnish_Two());

    }

    @Override
    public int getItemCount() {
        return timeSlotModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {


        private TextView mName;
        private TextView mDrink;
        private TextView mGarnish1;
        private TextView mGarnish2;



        public Viewholder(@NonNull View itemView) {
            super(itemView);

            mName = itemView.findViewById(R.id.mName);
            mDrink = itemView.findViewById(R.id.mDrink);
            mGarnish1 = itemView.findViewById(R.id.mGarnish1);
            mGarnish2 = itemView.findViewById(R.id.mGarnish2);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }




}