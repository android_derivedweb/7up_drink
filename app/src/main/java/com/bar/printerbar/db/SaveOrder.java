package com.bar.printerbar.db;

public class SaveOrder {
    private String name;
    private String email;
    private String phone;
    private String drink;
    private String garnish_One;
    private String garnish_Two;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    public String getDrink() {
        return drink;
    }

    public void setGarnish_One(String garnish_one) {
        this.garnish_One = garnish_one;
    }

    public String getGarnish_One() {
        return garnish_One;
    }

    public void setGarnish_Two(String garnish_two) {
        this.garnish_Two = garnish_two;
    }

    public String getGarnish_Two() {
        return garnish_Two;
    }
}
