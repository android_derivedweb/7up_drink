package com.bar.printerbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class Section_3 extends AppCompatActivity {
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-zA-Z0-9._-]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sec_3);

        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finaly change the color
        window.setStatusBarColor(ContextCompat.getColor(Section_3.this, R.color.teal_200));
        Bundle extras = getIntent().getExtras();
        String newString = extras.getString("mName");

        EditText mMobile = findViewById(R.id.mMobile);
        EditText mEmail = findViewById(R.id.mEmail);

        findViewById(R.id.btn_aceptar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             /*   if (mEmail.getText().toString().isEmpty() && mMobile.getText().toString().isEmpty()) {
                    Toast.makeText(Section_3.this, "enter your email or mobile number", Toast.LENGTH_LONG).show();
                } else if (!mEmail.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(Section_3.this,"invalid_address", Toast.LENGTH_LONG).show();
                }else if (mMobile.getText().toString().length() <= 9) {
                    Toast.makeText(Section_3.this,"invalid_mobile", Toast.LENGTH_LONG).show();
                }else {
                    startActivity(new Intent(Section_3.this,Section_4.class)
                            .putExtra("mName",newString)
                            .putExtra("mMobile",mMobile.getText().toString())
                            .putExtra("mEmail",mEmail.getText().toString())
                    );
                    finish();
                }*/

                if (mEmail.getText().toString().isEmpty() && mMobile.getText().toString().isEmpty()) {
                    Toast.makeText(Section_3.this, "enter your email or mobile number", Toast.LENGTH_LONG).show();

                } else {

                    if (!mEmail.getText().toString().isEmpty()) {
                        if (!mEmail.getText().toString().trim().matches(emailPattern)) {
                            Toast.makeText(Section_3.this, "invalid_address", Toast.LENGTH_LONG).show();
                        } else {
                            startActivity(new Intent(Section_3.this, Section_4.class)
                                    .putExtra("mName", newString)
                                    .putExtra("mMobile", mMobile.getText().toString())
                                    .putExtra("mEmail", mEmail.getText().toString())
                            );
                            finish();
                        }
                    } else {
                        if (mMobile.getText().toString().length() <= 9) {
                            Toast.makeText(Section_3.this, "invalid_mobile", Toast.LENGTH_LONG).show();
                        } else {
                            startActivity(new Intent(Section_3.this, Section_4.class)
                                    .putExtra("mName", newString)
                                    .putExtra("mMobile", mMobile.getText().toString())
                                    .putExtra("mEmail", mEmail.getText().toString())
                            );
                            finish();
                        }
                    }
                }

            }
        });

    }

}