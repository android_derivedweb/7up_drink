package com.bar.printerbar;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;

public class Section_5 extends AppCompatActivity {
    TextView txt1,txt2,txt3,txt4,txt5,txt6,txt7;
    ImageView img1,img2,img3,img4,img5,img6,img7;
    LinearLayout mLayout1,mLayout2,mLayout3,mLayout4,mLayout5,mLayout6,mLayout7;

    boolean mSelected_1 = false;
    boolean mSelected_2 = false;
    boolean mSelected_3 = false;
    boolean mSelected_4 = false;
    boolean mSelected_5 = false;
    boolean mSelected_6 = false;
    boolean mSelected_7 = false;


    ArrayList<String> stringArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sec_5);

        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finaly change the color
        window.setStatusBarColor(ContextCompat.getColor(Section_5.this, R.color.teal_200));


       /* findViewById(R.id.sdad).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Section_5.this,Section_6.class));
            }
        });*/

        Bundle extras = getIntent().getExtras();
        String mName = extras.getString("mName");
        String mMobile = extras.getString("mMobile");
        String mEmail = extras.getString("mEmail");
        String mDrink = extras.getString("mDrink");


        txt1 = findViewById(R.id.txt1);
        txt2 = findViewById(R.id.txt2);
        txt3 = findViewById(R.id.txt3);
        txt4 = findViewById(R.id.txt4);
        txt5 = findViewById(R.id.txt5);
        txt6 = findViewById(R.id.txt6);
        txt7 = findViewById(R.id.txt7);

        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        img4 = findViewById(R.id.img4);
        img5 = findViewById(R.id.img5);
        img6 = findViewById(R.id.img6);
        img7 = findViewById(R.id.img7);

        mLayout1 = findViewById(R.id.mLayout1);
        mLayout2 = findViewById(R.id.mLayout2);
        mLayout3 = findViewById(R.id.mLayout3);
        mLayout4 = findViewById(R.id.mLayout4);
        mLayout5 = findViewById(R.id.mLayout5);
        mLayout6 = findViewById(R.id.mLayout6);
        mLayout7 = findViewById(R.id.mLayout7);





        mLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!mSelected_1){

                    mSelected_1 = true;
                    txt1.setBackground(getResources().getDrawable(R.drawable.selected_layout));
                    stringArrayList.add(txt1.getText().toString());

                    if(stringArrayList.size() == 2){

                        String mFinalString = stringArrayList.get(0).toString() + "," + stringArrayList.get(1);
                        startActivity(new Intent(Section_5.this,Section_6.class)
                                .putExtra("mName",mName)
                                .putExtra("mMobile",mMobile)
                                .putExtra("mEmail",mEmail)
                                .putExtra("mDrink",mDrink)
                                .putExtra("mGarnish",mFinalString
                                )
                        );
                        finish();
                    }
                }else {

                    mSelected_1 = false;
                    txt1.setBackground(getResources().getDrawable(R.drawable.unselected_layout));
                    stringArrayList.remove(txt1.getText().toString());
                }

            }
        });

        mLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!mSelected_2){

                    mSelected_2 = true;
                    txt2.setBackground(getResources().getDrawable(R.drawable.selected_layout));
                    stringArrayList.add(txt2.getText().toString());

                    if(stringArrayList.size() == 2){
                        String mFinalString = stringArrayList.get(0).toString() + "," + stringArrayList.get(1);
                        startActivity(new Intent(Section_5.this,Section_6.class)
                                .putExtra("mName",mName)
                                .putExtra("mMobile",mMobile)
                                .putExtra("mEmail",mEmail)
                                .putExtra("mDrink",mDrink)
                                .putExtra("mGarnish",mFinalString
                                )
                        );
                        finish();                    }
                }else {

                    mSelected_2 = false;
                    txt2.setBackground(getResources().getDrawable(R.drawable.unselected_layout));
                    stringArrayList.remove(txt2.getText().toString());
                }

            }
        });

        mLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!mSelected_3){

                    mSelected_3 = true;
                    txt3.setBackground(getResources().getDrawable(R.drawable.selected_layout));
                    stringArrayList.add(txt3.getText().toString());

                    if(stringArrayList.size() == 2){
                        String mFinalString = stringArrayList.get(0).toString() + "," + stringArrayList.get(1);
                        startActivity(new Intent(Section_5.this,Section_6.class)
                                .putExtra("mName",mName)
                                .putExtra("mMobile",mMobile)
                                .putExtra("mEmail",mEmail)
                                .putExtra("mDrink",mDrink)
                                .putExtra("mGarnish",mFinalString
                                )
                        );
                        finish();                    }
                }else {

                    mSelected_3 = false;
                    txt3.setBackground(getResources().getDrawable(R.drawable.unselected_layout));
                    stringArrayList.remove(txt3.getText().toString());
                }

            }
        });

        mLayout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!mSelected_4){

                    mSelected_4 = true;
                    txt4.setBackground(getResources().getDrawable(R.drawable.selected_layout));
                    stringArrayList.add(txt4.getText().toString());

                    if(stringArrayList.size() == 2){
                        String mFinalString = stringArrayList.get(0).toString() + "," + stringArrayList.get(1);
                        startActivity(new Intent(Section_5.this,Section_6.class)
                                .putExtra("mName",mName)
                                .putExtra("mMobile",mMobile)
                                .putExtra("mEmail",mEmail)
                                .putExtra("mDrink",mDrink)
                                .putExtra("mGarnish",mFinalString
                                )
                        );
                        finish();                    }
                }else {

                    mSelected_4 = false;
                    txt4.setBackground(getResources().getDrawable(R.drawable.unselected_layout));
                    stringArrayList.remove(txt4.getText().toString());
                }

            }
        });

        mLayout5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!mSelected_5){

                    mSelected_5 = true;
                    txt5.setBackground(getResources().getDrawable(R.drawable.selected_layout));
                    stringArrayList.add(txt5.getText().toString());

                    if(stringArrayList.size() == 2){
                        String mFinalString = stringArrayList.get(0).toString() + "," + stringArrayList.get(1);
                        startActivity(new Intent(Section_5.this,Section_6.class)
                                .putExtra("mName",mName)
                                .putExtra("mMobile",mMobile)
                                .putExtra("mEmail",mEmail)
                                .putExtra("mDrink",mDrink)
                                .putExtra("mGarnish",mFinalString
                                )
                        );
                        finish();                    }
                }else {

                    mSelected_5 = false;
                    txt5.setBackground(getResources().getDrawable(R.drawable.unselected_layout));
                    stringArrayList.remove(txt5.getText().toString());
                }

            }
        });

        mLayout6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!mSelected_6){

                    mSelected_6 = true;
                    txt6.setBackground(getResources().getDrawable(R.drawable.selected_layout));
                    stringArrayList.add(txt6.getText().toString());

                    if(stringArrayList.size() == 2){
                        String mFinalString = stringArrayList.get(0).toString() + "," + stringArrayList.get(1);
                        startActivity(new Intent(Section_5.this,Section_6.class)
                                .putExtra("mName",mName)
                                .putExtra("mMobile",mMobile)
                                .putExtra("mEmail",mEmail)
                                .putExtra("mDrink",mDrink)
                                .putExtra("mGarnish",mFinalString
                                )
                        );
                        finish();                    }
                }else {

                    mSelected_6 = false;
                    txt6.setBackground(getResources().getDrawable(R.drawable.unselected_layout));
                    stringArrayList.remove(txt6.getText().toString());
                }

            }
        });

        mLayout7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!mSelected_7){

                    mSelected_7 = true;
                    txt7.setBackground(getResources().getDrawable(R.drawable.selected_layout));
                    stringArrayList.add(txt7.getText().toString());

                    if(stringArrayList.size() == 2){
                        String mFinalString = stringArrayList.get(0).toString() + "," + stringArrayList.get(1);
                        startActivity(new Intent(Section_5.this,Section_6.class)
                                .putExtra("mName",mName)
                                .putExtra("mMobile",mMobile)
                                .putExtra("mEmail",mEmail)
                                .putExtra("mDrink",mDrink)
                                .putExtra("mGarnish",mFinalString
                                )
                        );
                        finish();                    }
                }else {

                    mSelected_7 = false;
                    txt7.setBackground(getResources().getDrawable(R.drawable.unselected_layout));
                    stringArrayList.remove(txt7.getText().toString());
                }

            }
        });

      findViewById(R.id.mNext78).setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {

              if(stringArrayList.isEmpty()){
                  String mFinalString = "";
                  startActivity(new Intent(Section_5.this,Section_6.class)
                          .putExtra("mName",mName)
                          .putExtra("mMobile",mMobile)
                          .putExtra("mEmail",mEmail)
                          .putExtra("mDrink",mDrink)
                          .putExtra("mGarnish",mFinalString
                          )
                  );
                  finish();
              }else {
                  String mFinalString = stringArrayList.get(0).toString();
                  startActivity(new Intent(Section_5.this,Section_6.class)
                          .putExtra("mName",mName)
                          .putExtra("mMobile",mMobile)
                          .putExtra("mEmail",mEmail)
                          .putExtra("mDrink",mDrink)
                          .putExtra("mGarnish",mFinalString
                          )
                  );
                  finish();
              }

          }
      });

    }

}